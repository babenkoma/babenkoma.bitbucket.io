function getRandom () {
	return Math.round(Math.random() * 100);
}

function getDate (text) {
	let date = new Date(text);

	let dd = date.getDate();
	if (dd < 10) {
		dd = '0' + dd;
	}

	let mm = date.getMonth() + 1;
	if (mm < 10) {
		mm = '0' + mm;
	}

	let yyyy = date.getFullYear();
	if (yyyy < 10) {
		yyyy = '0' + yyyy;
	}

	return dd + '.' + mm + '.' + yyyy;
}

function getClearPhone (text) {
	return text.replace(/([-()]|\s)*/g, '');
}

function usersInit () {
	let $container = $('[data-users]');
	let $list = $container.find('[data-users-list]');
	let $statistic = $container.find('[data-users-statistic]');
	let $filter = $container.find('[data-users-filter]');

	let options = $container.data('users');
	let users = [];
	let filterUsers = [];

	const getUsers = () => {
		let html = '';
		filterUsers.forEach((user) => {
			html += `<div class="user-list__item">${getUserCard(user)}</div>`;
		});
		if (!html) {
			return `<div class="no-user">${window.translate.noUsers}</div>`;
		} else {
			return `<div class="user-list">${html}</div>`;
		}
	};

	const getUserCard = (user) => {
		return `<div class="user">
			<div class="user__image"><img src="${user.picture.large}" alt=""></div>
			<div class="user__name">${user.name.title}. ${user.name.first} ${user.name.last}, ${user.gender}</div>
			<div class="user__attribute">
				<div class="user__key">${window.translate.phone}:</div>
				<a href="tel:${getClearPhone(user.phone)}" class="user__link">${user.phone}</a>
			</div>
			<div class="user__attribute">
				<div class="user__key">Email:</div>
				<a href="mailto:${user.email}" class="user__link">${user.email}</a>
			</div>
			<div class="user__attribute">
				<div class="user__key">${window.translate.address}:</div>
				<div class="user__text">${user.location.country}, ${user.location.state}, ${user.location.city}, ${user.location.street.name}, ${user.location.street.number}</div>
			</div>
			<div class="user__attribute">
				<div class="user__key">${window.translate.birthday}:</div>
				<div class="user__text">${getDate(user.dob.date)} г. (${user.dob.age} лет)</div>
			</div>
			<div class="user__attribute">
				<div class="user__key">${window.translate.regDate}:</div>
				<div class="user__text">${getDate(user.registered.date)} г.</div>
			</div>
		</div>`;
	};

	const getStatistic = () => {
		let countUsers = filterUsers.length;
		let countMales = 0;
		let countFemales = 0;
		let natsList = {};
		let nats = '';

		filterUsers.forEach((item) => {
			if (item.gender === 'male') {
				countMales++;
			} else {
				countFemales++;
			}

			if (natsList[item.nat]) {
				natsList[item.nat]++;
			} else {
				natsList[item.nat] = 1;
			}
		});

		let genderText = countMales > countFemales ? 'мужчин' : (countFemales > countMales ? 'женщин' : 'поровну');

		for (let key in natsList) {
			nats += (nats ? ', ' : '') + key + ' - ' + natsList[key];
		}

		return filterUsers.length ? `<div class="statistic">
			<div class="statistic__title">${window.translate.statTitle}</div>
			<div class="statistic__row">
				<div class="statistic__key">${window.translate.userCount}:</div>
				<div class="statistic__value">${countUsers}</div>
			</div>
			<div class="statistic__row">
				<div class="statistic__key">${window.translate.maleCount}:</div>
				<div class="statistic__value">${countMales}</div>
			</div>
			<div class="statistic__row">
				<div class="statistic__key">${window.translate.femaleCount}:</div>
				<div class="statistic__value">${countFemales}</div>
			</div>
			<div class="statistic__row">
				<div class="statistic__key">${window.translate.maleOrFemale}:</div>
				<div class="statistic__value">${genderText}</div>
			</div>
			<div class="statistic__row">
				<div class="statistic__text">${window.translate.natCount}:</div>
			</div>
			<div class="statistic__row">
				<div class="statistic__value">${nats}</div>
			</div>
		</div>` : '';
	};

	const getFilter = () => {
		return users.length ? `<form class="filter" data-filter-form>
			<div class="filter__row">
				<div class="filter__control">
					<input type="text" placeholder="${window.translate.search}" class="filter__input" name="query">
					<button type="button" class="filter__icon" data-filter-clear>
						<img src="images/delete.svg" alt="">
					</button>
				</div>
			</div>
			<div class="filter__row">
				<div class="filter__control">
					<select class="filter__select" name="gender">
						<option value="">${window.translate.genderText}</option>
						<option value="male">${window.translate.maleText}</option>
						<option value="female">${window.translate.femaleText}</option>
					</select>
				</div>
				<div class="filter__control">
					<select class="filter__select" name="age">
						<option value="">${window.translate.ageText}</option>
						<option value="<35">${window.translate.younger} 35</option>
						<option value="35-40">${window.translate.from} 35 ${window.translate.to} 40</option>
						<option value="40-45">${window.translate.from} 40 ${window.translate.to} 45</option>
						<option value=">45">${window.translate.over} 45</option>
					</select>
				</div>
				<div class="filter__control">
					<select class="filter__select" name="nat">
						<option value="">${window.translate.natText}</option>
						${users.map((user) => (`
							<option value="${user.nat}">${user.nat}</option>
						`))}
					</select>
				</div>
				<div class="filter__control">
					<select class="filter__select" name="sort">
						<option value="">${window.translate.sortText}</option>
						<option value="abc">${window.translate.abcText}</option>
						<option value="genderAsk">${window.translate.genderAskText}</option>
						<option value="genderDesk">${window.translate.genderDeskText}</option>
						<option value="genderAskAbc">${window.translate.genderAskAbcText}</option>
						<option value="genderDeskAbc">${window.translate.genderDeskAbcText}</option>
					</select>
				</div>
			</div>
		</form>` : '';
	};

	$(document).on('click', '[data-users-button]', (event) => {
		let $this = $(event.target);

		$container.addClass('is-pending');

		$.ajax({
			url: options.url,
			data: {
				results: getRandom()
			},
			dataType: 'json'
		}).done((response) => {
			users = response.results;
			filterUsers = response.results;

			$filter.html(getFilter());
			$list.html(getUsers());
			$statistic.html(getStatistic());
		}).fail((error) => {
			console.error(error);
		}).always(function () {
			$container.removeClass('is-pending');
		});
	});

	$(document).on('submit', '[data-filter-form]', (event) => {
		event.preventDefault();

		let $this = $(event.target);

		let query = $this.find('[name="query"]').val().toLowerCase();
		let sort = $this.find('[name="sort"]').val();
		let gender = $this.find('[name="gender"]').val();
		let age = $this.find('[name="age"]').val();
		let nat = $this.find('[name="nat"]').val();

		filterUsers = users.filter((user) => {
			let name = (user.name.first + user.name.last).toLowerCase();
			return (name.indexOf(query) !== -1 || getClearPhone(user.phone).indexOf(getClearPhone(query)) !== -1 || user.email.indexOf(query)) !== -1;
		});

		if (gender) {
			filterUsers = filterUsers.filter((user) => {
				return (user.gender === gender);
			});
		}

		if (age) {
			filterUsers = filterUsers.filter((user) => {
				if (age.indexOf('-') !== -1) {
					return (user.dob.age > parseInt(age.split('-')[0]) && user.dob.age <= parseInt(age.split('-')[1]));
				} else if (age.indexOf('<') !== -1) {
					return (user.dob.age <= parseInt(age.split('<')[1]));
				} else if (age.indexOf('>') !== -1) {
					return (user.dob.age > parseInt(age.split('>')[1]));
				}
			});
		}

		if (nat) {
			filterUsers = filterUsers.filter((user) => {
				return (user.nat === nat);
			});
		}

		if (sort === 'abc') {
			filterUsers = filterUsers.sort((a, b) => {
				let name1 = a.name.first + ' ' + a.name.last;
				let name2 = b.name.first + ' ' + b.name.last;
				return ('' + name1).localeCompare(name2);
			});
		} else if (sort === 'genderAsk') {
			filterUsers = filterUsers.sort((a, b) => {
				let name1 = a.gender;
				let name2 = b.gender;
				return ('' + name1).localeCompare(name2);
			});
		} else if (sort === 'genderDesk') {
			filterUsers = filterUsers.sort((a, b) => {
				let name1 = a.gender;
				let name2 = b.gender;
				return !('' + name1).localeCompare(name2);
			});
		} else if (sort === 'genderAskAbc') {
			let filterUsersFemale = filterUsers.filter((user) => user.gender === 'female').sort((a, b) => {
				let name1 = a.name.first + ' ' + a.name.last;
				let name2 = b.name.first + ' ' + b.name.last;
				return ('' + name1).localeCompare(name2);
			});

			let filterUsersMale = filterUsers.filter((user) => user.gender === 'male').sort((a, b) => {
				let name1 = a.name.first + ' ' + a.name.last;
				let name2 = b.name.first + ' ' + b.name.last;
				return ('' + name1).localeCompare(name2);
			});

			filterUsers = filterUsersFemale.concat(filterUsersMale);
		} else if (sort === 'genderDeskAbc') {
			let filterUsersFemale = filterUsers.filter((user) => user.gender === 'female').sort((a, b) => {
				let name1 = a.name.first + ' ' + a.name.last;
				let name2 = b.name.first + ' ' + b.name.last;
				return ('' + name1).localeCompare(name2);
			});

			let filterUsersMale = filterUsers.filter((user) => user.gender === 'male').sort((a, b) => {
				let name1 = a.name.first + ' ' + a.name.last;
				let name2 = b.name.first + ' ' + b.name.last;
				return ('' + name1).localeCompare(name2);
			});

			filterUsers = filterUsersMale.concat(filterUsersFemale);
		}

		$list.html(getUsers());
		$statistic.html(getStatistic());
	});

	$(document).on('click', '[data-filter-clear]', (event) => {
		let $this = $(event.target);
		let $form = $this.closest('form');
		let $query = $form.find('[name="query"]');
		$query.val('');
		$form.trigger('submit');
	});

	$(document).on('keyup', 'input[type="text"]', (event) => {
		$(event.target).closest('form').trigger('submit');
	});

	$(document).on('change', 'input,select', (event) => {
		$(event.target).closest('form').trigger('submit');
	});
}

$(document).ready(function () {
	usersInit();
});
